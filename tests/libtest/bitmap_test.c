#include <bitmap.h>

#define CTEST_MAIN
#define CTEST_SEGFAULT
#include <ctest.h>

CTEST(BitMapTest, BaseFuncs1) {
    struct BitMap bitmap = { 0 };

    ASSERT_EQUAL(BitMap__init(&bitmap), 0);

    {
        ASSERT_EQUAL(BitMap__push(&bitmap, true), 0);
        ASSERT_EQUAL(BitMap__push(&bitmap, false), 0);
        ASSERT_EQUAL(BitMap__push(&bitmap, false), 0);
        ASSERT_EQUAL(BitMap__push(&bitmap, true), 0);
        ASSERT_EQUAL(BitMap__push(&bitmap, true), 0);

        ASSERT_EQUAL(BitMap__len(&bitmap), 5);
    }

    {
        bool value = false;

        ASSERT_EQUAL(BitMap__get(&bitmap, 0, &value), 0);
        ASSERT_EQUAL(value, true);

        ASSERT_EQUAL(BitMap__get(&bitmap, 1, &value), 0);
        ASSERT_EQUAL(value, false);

        ASSERT_EQUAL(BitMap__get(&bitmap, 2, &value), 0);
        ASSERT_EQUAL(value, false);

        ASSERT_EQUAL(BitMap__get(&bitmap, 3, &value), 0);
        ASSERT_EQUAL(value, true);
        
        ASSERT_EQUAL(BitMap__get(&bitmap, 4, &value), 0);
        ASSERT_EQUAL(value, true);
    }
    

    ASSERT_EQUAL(BitMap__fini(&bitmap), 0);
}

CTEST(BitMapTest, BaseFuncs2) {
    struct BitMap bitmap = { 0 };

    ASSERT_EQUAL(BitMap__init(&bitmap), 0);

    ASSERT_EQUAL(BitMap__capacity(&bitmap), 32);
    ASSERT_EQUAL(BitMap__resize(&bitmap, 15, false), 0);
    ASSERT_EQUAL(BitMap__len(&bitmap), 15);

    {
        ASSERT_EQUAL(BitMap__set(&bitmap, 3, true), 0);
        ASSERT_EQUAL(BitMap__set(&bitmap, 7, true), 0);
        ASSERT_EQUAL(BitMap__set(&bitmap, 17, true), -2);
    }

    {
        bool value = false;

        ASSERT_EQUAL(BitMap__get(&bitmap, 3, &value), 0);
        ASSERT_EQUAL(value, true);

        ASSERT_EQUAL(BitMap__get(&bitmap, 7, &value), 0);
        ASSERT_EQUAL(value, true);

        ASSERT_EQUAL(BitMap__get(&bitmap, 17, &value), -3);

        ASSERT_EQUAL(BitMap__get(&bitmap, 5, &value), 0);
        ASSERT_EQUAL(value, false);
    }

    ASSERT_EQUAL(BitMap__fini(&bitmap), 0);
}

CTEST(BitMapTest, BaseFuncs3) {
    struct BitMap bitmap = { 0 };

    ASSERT_EQUAL(BitMap__init(&bitmap), 0);

    ASSERT_EQUAL(BitMap__resize(&bitmap, 5, true), 0);
    ASSERT_EQUAL(BitMap__len(&bitmap), 5);

    {
        bool value = false;

        ASSERT_EQUAL(BitMap__get(&bitmap, 0, &value), 0);
        ASSERT_EQUAL(value, true);

        ASSERT_EQUAL(BitMap__get(&bitmap, 1, &value), 0);
        ASSERT_EQUAL(value, true);

        ASSERT_EQUAL(BitMap__get(&bitmap, 2, &value), 0);
        ASSERT_EQUAL(value, true);

        ASSERT_EQUAL(BitMap__get(&bitmap, 3, &value), 0);
        ASSERT_EQUAL(value, true);

        ASSERT_EQUAL(BitMap__get(&bitmap, 4, &value), 0);
        ASSERT_EQUAL(value, true);
    }

    {
        bool value = false;

        ASSERT_EQUAL(BitMap__pop(&bitmap, &value), 0);
        ASSERT_EQUAL(value, true);
        ASSERT_EQUAL(BitMap__len(&bitmap), 4);
    }

    {
        ASSERT_EQUAL(BitMap__capacity(&bitmap), 32);
        ASSERT_EQUAL(BitMap__shrinktofit(&bitmap), 0);
        
        ASSERT_EQUAL(BitMap__capacity(&bitmap), 32);
    }

    ASSERT_EQUAL(BitMap__fini(&bitmap), 0);
}

int main(int argc, char const *argv[])
{
    int result = ctest_main(argc, argv);

    return result;
}