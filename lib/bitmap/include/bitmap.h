#ifndef _BITMAP_H
#define _BITMAP_H

#include <stdio.h>
#include <stdbool.h>
#include <malloc.h>

enum
{
    INITIAL_SIZE = 4,
};

typedef char byte_t;

struct BitMap
{
    byte_t *data;
    size_t  len;
};

/* BitMap initializer and finalizer */
int BitMap__init(struct BitMap *const self);
int BitMap__fini(struct BitMap *const self);

/* BitMap size */
size_t BitMap__len(struct BitMap const *const self);
size_t BitMap__capacity(struct BitMap const *const self);
size_t BitMap__sizeof(struct BitMap const *const self);
bool BitMap__isempty(struct BitMap const *const self);

/* BitMap size manipulation */
int BitMap__resize(struct BitMap *const self, size_t const new_size, bool const value);
int BitMap__clear(struct BitMap *const self);

/* BitMap capacity manipulation */
int BitMap__reserve(struct BitMap *const self, size_t const new_capacity);
int BitMap__shrinktofit(struct BitMap *const self);

/* Add new bit to the end of BitMap or pop from the end */
int BitMap__push(struct BitMap *const self, bool const value);
int BitMap__pop(struct BitMap *const self, bool *const value);

/* Get bit value by index */
int BitMap__get(
    struct BitMap const *const self,
    size_t               const index,
    bool                *const value);

/* Set bit value by index */
int BitMap__set(
    struct BitMap *const self,
    size_t         const index,
    bool           const value);

int BitMap__init(struct BitMap *const self)
{
    if (NULL == self)
    {
        return -1;
    }

    self->len = 0;
    self->data = malloc(INITIAL_SIZE);

    return 0;
}

int BitMap__fini(struct BitMap *const self)
{
    if (NULL == self)
    {
        return -1;
    }

    self->len = 0;
    free(self->data);

    return 0;
}


size_t BitMap__len(struct BitMap const *const self)
{
    if (NULL == self)
    {
        return -1;
    }

    return self->len;
}


size_t BitMap__capacity(struct BitMap const *const self)
{
    if (NULL == self)
    {
        return -1;
    }

    size_t initial_bytes = INITIAL_SIZE;
    size_t owned_bytes = (self->len / 8) + ((self->len % 8) != 0);

    return (owned_bytes > initial_bytes)
           ? (owned_bytes * 8)
           : (initial_bytes * 8);
}


size_t BitMap__sizeof(struct BitMap const *const self)
{
    if (NULL == self)
    {
        return -1;
    }

    size_t const capacity = BitMap__capacity(self);

    return (capacity / 8) + sizeof(*self);
}


bool BitMap__isempty(struct BitMap const *const self)
{
    if (NULL == self)
    {
        return true;
    }

    return BitMap__len(self) == 0;
}

int BitMap__resize(struct BitMap *const self, size_t const new_size, bool const value)
{
    if (NULL == self)
    {
        return -1;
    }

    if (BitMap__capacity(self) < new_size)
    {
        return -2;
    }

    /* if need to add more places */
    if (BitMap__len(self) < new_size)
    {
        size_t const old_size = self->len == 0 ? 1 : self->len;
        self->len = new_size;

        for (size_t i = old_size - 1; i < new_size; i++)
        {
            if (BitMap__set(self, i, value) < 0)
            {
                return -3;
            }
        }
    }
    /* if we need to cut BitMap */
    else if (BitMap__len(self) > new_size)
    {
        self->len = new_size;
    }

    return 0;
}

int BitMap__clear(struct BitMap *const self)
{
    return BitMap__resize(self, 0, false);
}

int BitMap__reserve(struct BitMap *const self, size_t const new_capacity)
{
    if (NULL == self)
    {
        return -1;
    }

    size_t const old_capacity_bytes = BitMap__capacity(self) / 8;
    size_t new_capacity_bytes = new_capacity / 8;

    if (new_capacity_bytes < INITIAL_SIZE)
    {
        new_capacity_bytes = INITIAL_SIZE;
    }

    if (new_capacity_bytes != old_capacity_bytes)
    {
        self->data = realloc(self->data, new_capacity_bytes);
    }

    return 0;
}

int BitMap__shrinktofit(struct BitMap *const self)
{
    return BitMap__reserve(self, BitMap__len(self));
}

int BitMap__push(struct BitMap *const self, bool const value)
{
    if (NULL == self)
    {
        return -1;
    }

    size_t capacity = BitMap__capacity(self);

    if (self->len == capacity)
    {
        /* If bitmap has not enough place, allocate 1 more byte */
        BitMap__reserve(self, capacity / 8 + 1);
    }

    self->len += 1;

    BitMap__set(self, self->len - 1, value);

    return 0;
}

int BitMap__pop(struct BitMap *const self, bool *const value)
{
    if (NULL == self)
    {
        return -1;
    }

    if (NULL == value)
    {
        return -2;
    }

    if (0 == self->len)
    {
        return -3;
    }

    BitMap__get(self, self->len - 1, value);

    self->len -= 1;

    return 0;
}

int BitMap__get(struct BitMap const *const self, size_t const index, bool *const value)
{
    if (NULL == self)
    {
        return -1;
    }

    if (NULL == value)
    {
        return -2;
    }

    if (index >= self->len)
    {
        return -3;
    }

    /* pointer to byte where bit is */
    byte_t *byte = &self->data[index / 8];

    /* bit index in byte */
    size_t bit = index % 8;

    *value = ((*byte >> bit) & 1) == 1;

    return 0;
}

int BitMap__set(struct BitMap *const self, size_t const index, bool const value)
{
    if (NULL == self)
    {
        return -1;
    }

    if (index >= self->len)
    {
        return -2;
    }

    /* pointer to byte where bit is */
    byte_t *byte = &self->data[index / 8];

    /* bit index in byte */
    size_t bit = index % 8;

    if (value)
    {
        *byte |= 1 << bit;
    }
    else
    {
        *byte &= ~(1 << bit);
    }  

    return 0;
}

#endif // _BITMAP_H